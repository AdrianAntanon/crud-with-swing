package practica5;

import org.postgresql.util.PSQLException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.*;

class AddPersonal extends JFrame {

    public AddPersonal() throws SQLException {
        setTitle("Añadir producto");
        setBounds(1300, 200, 350, 370);
        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(7, 1, 1, 5));
        panel.setBorder(new EmptyBorder(15, 15, 15, 15));

        String[] answers = new String[6];

        Statement pregunta = HumanResourcesPane.connection.createStatement();
        ResultSet resposta = pregunta.executeQuery("SELECT * FROM Productos ORDER BY 1");
        ResultSetMetaData rsmeta = resposta.getMetaData();
        int columns = rsmeta.getColumnCount();

        String [] dataFields = new String[6];

        for (int index = 1; index <= columns; index++){
            String columnName = rsmeta.getColumnLabel(index);

            dataFields[index-1] = columnName;
        }

        JTextField idProduct = new JTextField();
        JTextField productName = new JTextField();
        JTextField productBrand = new JTextField();
        JTextField productPrice = new JTextField();
        JTextField productPVP = new JTextField();
        JTextField productImg = new JTextField();

        for (int index = 0; index < dataFields.length; index++) {
            panel.add(createQuestionTextField(dataFields[index]));

            switch (index){
                case 0:
                    panel.add(idProduct);
                    break;
                case 1:
                    panel.add(productName);
                    break;
                case 2:
                    panel.add(productBrand);
                    break;
                case 3:
                    panel.add(productPrice);
                    break;
                case 4:
                    panel.add(productPVP);
                    break;
                case 5:
                    panel.add(productImg);
                    break;
            }
        }

        JPanel buttonPanel = new JPanel();
        JButton buttonOk = new JButton("OK");
        buttonOk.addActionListener(actionEvent -> {
            int id =  Integer.parseInt(idProduct.getText());
            String name = productName.getText();
            String brand = productBrand.getText();
            int price = Integer.parseInt(productPrice.getText());
            int pvp = Integer.parseInt(productPVP.getText());
            String img = productImg.getText();

            try {
//                AÑADIR DATOS ( C )
                PreparedStatement query = HumanResourcesPane.connection.prepareStatement("INSERT INTO productos VALUES(?, ?, ?, ?, ?, ?)");
                query.setInt(1,id);
                query.setString(2,name);
                query.setString(3,brand);
                query.setInt(4,price);
                query.setInt(5,pvp);
                query.setString(6,img);
                query.executeUpdate();
            } catch (PSQLException error) {
                error.getMessage();
            }catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            answers[0] = idProduct.getText();
            answers[1] = productName.getText();
            answers[2] = productBrand.getText();
            answers[3] = productPrice.getText();
            answers[4] = productPVP.getText();
            answers[5] = productImg.getText();

            HumanResourcesPane.tableModel.addRow(answers);
        });
        JButton buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(actionEvent -> dispose());


        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
        buttonPanel.add(buttonOk, BorderLayout.WEST);
        buttonPanel.add(buttonCancel, BorderLayout.EAST);

        panel.add(buttonPanel);

        add(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public JTextField createQuestionTextField(String title){

        if (title.equalsIgnoreCase("")){
            return new JTextField();
        }else {
            Font font = new Font(Font.SANS_SERIF, Font.BOLD, 11);

            JTextField question = new JTextField(title);
            question.setBackground(null);
            question.setFont(font);
            question.setBorder(null);

            return question;
        }


    }

}
package practica5;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;

class MyJFrame extends JFrame {

    public MyJFrame(String title) throws HeadlessException, IOException, SQLException {
        super(title);

        setJMenuBar(new MyJMenuBar());

        MyJToolBar toolBar = new MyJToolBar();
        getContentPane().add(toolBar, BorderLayout.NORTH);
        pack();

        JPanel mainPanel = new JPanel();
        CardLayout cardLayout = new CardLayout();
        mainPanel.setLayout(cardLayout);

        mainPanel.add(new HumanResourcesPane(), "HumanResourcesPane");


        getContentPane().add(mainPanel);

        setBounds(800,800,800,800);
        setLocationRelativeTo(null);
    }

}
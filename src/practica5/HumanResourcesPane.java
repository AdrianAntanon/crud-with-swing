package practica5;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class HumanResourcesPane extends JPanel {
    protected static int rowSelected;
    protected static DefaultTableModel tableModel;
    protected static String user = "adrian-antanyon-7e3";
    protected static String key = "Contra123";
    protected static String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
    //Class.forName("org.postgresql.Driver");

    protected static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(urlDatabase, user, key);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public HumanResourcesPane() throws SQLException {
        setLayout(new BorderLayout());


        tableModel = new DefaultTableModel();
        JTable myOwnTable = new JTable(tableModel);

        Statement pregunta = connection.createStatement();
//        LEER LOS DATOS ( R )
        ResultSet resposta = pregunta.executeQuery("SELECT * FROM Productos ORDER BY 1");
        ResultSetMetaData rsmeta = resposta.getMetaData();
        int columns = rsmeta.getColumnCount();

        for (int index = 1; index <= columns; index++) {
            String columnName = rsmeta.getColumnLabel(index);

            tableModel.addColumn(columnName);
        }

        while (resposta.next()) {
            int id = resposta.getInt("id_producto");
            String idString = id + "";

            String name = resposta.getString("nombre");

            System.out.println(id + " " + name);


            String brand = resposta.getString("marca");

            int price = resposta.getInt("precio");
            String priceString = price + "";

            int pvp = resposta.getInt("pvp");
            String pvpString = pvp + "";

            String img = resposta.getString("img");

            String[] data = {idString, name, brand, priceString, pvpString, img};

            tableModel.addRow(data);

        }

        myOwnTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        myOwnTable.setFillsViewportHeight(true);

        myOwnTable.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                rowSelected = myOwnTable.getSelectedRow();
                int columnSelected = myOwnTable.getSelectedColumn();
                String columnName = myOwnTable.getColumnName(columnSelected);
                String valueToChange = (String) myOwnTable.getValueAt(rowSelected, columnSelected);
//                ACTUALIZAR LOS DATOS ( U )
                String sqlUpdate = "UPDATE productos SET " + columnName + " = ? WHERE id_producto = ?";
                System.out.println(sqlUpdate);
                System.out.println(columnSelected+1);

                if (columnSelected != 0) {
                    try {
                        PreparedStatement query = connection.prepareStatement(sqlUpdate);
                        if (columnSelected == 3 || columnSelected == 4) {
                            int valueToChangeInt = Integer.parseInt(valueToChange);
                            query.setInt(1, valueToChangeInt);
                        } else {
                            query.setString(1, valueToChange);
                        }
                        query.setInt(2, rowSelected+1);
                        query.executeUpdate();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }

            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
            }
        });



        JButton addNewHR = new JButton("Añadir nuevo personal");
        addNewHR.addActionListener(e -> {
            AddPersonal test = null;
            try {
                test = new AddPersonal();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            test.setVisible(true);
        });
        JPanel panelAddNewHR = new JPanel();
        panelAddNewHR.add(addNewHR);
        add(panelAddNewHR, BorderLayout.SOUTH);

        JScrollPane tableView = new JScrollPane(myOwnTable);
        add(tableView);
    }
}

package practica5;

import org.postgresql.util.PSQLException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class MyJToolBar extends JToolBar {
    static JButton addPersonal;
    static JButton deletePersonal;

    public MyJToolBar() throws IOException, SQLException {
        String ROUTE_IMG = "src/practica5/img/";
        String[] imgCategory = {"add", "delete"};
        String extensionFile = ".png";
        ImageIcon img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[0]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        AddPersonal newEmployee = new AddPersonal();
        newEmployee.setVisible(false);

        addPersonal = new JButton("Añadir", img);
        addPersonal.addActionListener(actionEvent -> newEmployee.setVisible(true));
        add(addPersonal);

        img = new ImageIcon(ImageIO.read(new File(ROUTE_IMG + imgCategory[1]+ extensionFile)).getScaledInstance(50, 50, Image.SCALE_SMOOTH));

        deletePersonal = new JButton("Eliminar", img);
        deletePersonal.addActionListener(actionEvent -> {
            Object id = HumanResourcesPane.tableModel.getValueAt(HumanResourcesPane.rowSelected, 0);
            int productId = Integer.parseInt((String) id);
            try {
//                ELIMINAR LOS DATOS ( D )
                PreparedStatement query = HumanResourcesPane.connection.prepareStatement("DELETE FROM productos WHERE id_producto=" + productId);
                query.executeUpdate();
            } catch (SQLException error) {
                error.getMessage();
            }

            HumanResourcesPane.tableModel.removeRow(HumanResourcesPane.rowSelected);
        });
        add(deletePersonal);


    }
}
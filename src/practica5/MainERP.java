package practica5;

import java.io.IOException;
import java.sql.SQLException;

public class MainERP {
    protected static MyJFrame myERP;

    public static void main(String[] args) throws IOException, SQLException {
        myERP = new MyJFrame("CRUD");

        myERP.setVisible(true);
        myERP.setDefaultCloseOperation(MyJFrame.EXIT_ON_CLOSE);
    }
}
